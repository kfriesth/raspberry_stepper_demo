#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui_(new Ui::MainWindow),
        motor_(new StepperMotor(4, 5))
{
  ui_->setupUi(this);
  /*// Works but not very beautiful
   qApp->setStyleSheet("QAbstractSpinBox::up-button { width: 40px; }"
   "QAbstractSpinBox::down-button { width: 40px; }");
   */
  this->showMaximized();

  // Connect angle
  connect(ui_->angle_dial, SIGNAL(sliderReleased()), this, SLOT(moveToAngle()));

  // Update loop
  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(updateAngle()));
  timer->start(100); // 10 Hz

  emit ui_->statusBar->showMessage("Ready to take commands", 3000);
}

MainWindow::~MainWindow()
{
  delete ui_;
}

void MainWindow::updateAngle()
{
  double angle_deg(motor_->getAngle());
  angle_deg *= 180.0 / M_PI;

  // Angle is constrained between -180 / +180, un-wrap it
  if (angle_deg < 0)
    angle_deg += 360;

  angle_deg = 360 - angle_deg;
  ui_->lcdNumber_position->display(QString("%1").arg(angle_deg, 0, 'f', 1));
}

void MainWindow::moveToAngle()
{
  std::chrono::nanoseconds step_delay(unsigned(ui_->spinBox_step_delay->value() * 1e6));
  double angle_rad(- ui_->angle_dial->value());
  emit ui_->statusBar->showMessage("Moving to " + QString::fromStdString(std::to_string(angle_rad)) + " degrees", 6000);
  angle_rad *= M_PI / 180.0;
  motor_->moveToAngle(angle_rad, step_delay, true, false, std::chrono::nanoseconds::zero(), true);
}

void MainWindow::on_pushButton_stop_clicked()
{
  motor_->moveSteps(0, false, std::chrono::nanoseconds::zero(), std::chrono::nanoseconds::zero(), true);
}

void MainWindow::on_pushButton_start_clicked()
{
  std::chrono::nanoseconds rev_duration(uint64_t(ui_->doubleSpinBox->value() * 1e9));
  const bool clockwise(ui_->checkBox_clockwise->isChecked());
  std::string clockwise_str(clockwise ? "clockwise" : "anti-clockwise");
  emit ui_->statusBar->showMessage(
      "Moving " + QString::fromStdString(clockwise_str) + ", 1 rotation in "
          + QString::fromStdString(std::to_string(ui_->doubleSpinBox->value()) + " sec"),
      6000);
  motor_->moveToVelocity(rev_duration, clockwise, std::chrono::nanoseconds::zero(), true);
}

void MainWindow::on_pushButton_reset_clicked()
{
  if (motor_->isMoving())
  {
    emit ui_->statusBar->showMessage("Cannot reset motor while moving!", 12000);
    return;
  }

  emit ui_->statusBar->showMessage("Motor angle reset", 6000);
  motor_->resetAngle();
}

