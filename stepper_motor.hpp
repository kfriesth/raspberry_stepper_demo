#ifndef STEPPER_MOTOR_HPP
#define STEPPER_MOTOR_HPP

#include <iostream>
#include <atomic>
#include <thread>
#include <chrono>
#include <cmath>
#include <math.h>

class StepperMotor
{
public:
  StepperMotor(const unsigned step_pin,
               const unsigned dir_pin);

  virtual ~StepperMotor();

  void configure(const double step_angle,
                 const double micro_stepping,
                 const double gearing_ratio);

  void resetAngle();

  bool isMoving();

  double getAngle();

  double getVelocity();

  unsigned getPercentageComplete();

  void stop(const bool blocking = false);

  void moveSteps(const unsigned steps,
                 const bool direction,
                 const std::chrono::nanoseconds &step_delay,
                 const std::chrono::nanoseconds &start_delay = std::chrono::nanoseconds(0),
                 const bool interupt = false);

  void moveToAngle(double target_angle,
                   const std::chrono::nanoseconds &time,
                   const bool use_step_delay,
                   const bool force_clockwise,
                   const std::chrono::nanoseconds &start_delay = std::chrono::nanoseconds(0),
                   const bool interupt = false);

  void moveToVelocity(const std::chrono::nanoseconds &revolution_duration,
                      const bool clockwise,
                      const std::chrono::nanoseconds &start_delay = std::chrono::nanoseconds(0),
                      const bool interupt = false);

  // Not mandatory
  std::string name_;

private:
  /**
   * Function to constrain angle between -Pi and Pi
   * @param theta
   */
  void constrainAngle(double &theta);

  /**
   * This actually moves the stepper motor if needed
   */
  void steppingCallback();

  void move(const uint64_t steps,
            const bool direction,
            const std::chrono::nanoseconds &step_delay,
            const std::chrono::nanoseconds &start_delay = std::chrono::nanoseconds(0),
            const bool infinite_rotation = false);

  const unsigned step_pin_;
  const unsigned dir_pin_;

  // Stepper motor angle
  std::atomic<double> current_angle_;
  // Stepper motor speed
  std::atomic<double> current_velocity_;
  // Is motor moving
  std::atomic<bool> is_moving_;
  // Percentage of the current command complete. 100 means motor has done the work
  std::atomic<unsigned> percentage_complete_;

  // Stepper motor properties
  // Check isMoving() to know if you can modify these values
  double step_angle_; // In radians
  double micro_stepping_; // 1/2 1/4 1/8 1/16 and 1/32 are standards
  double gearing_ratio_; // 1 if there is no gearing (= direct drive)

  // Stepper motor commands
  std::atomic<uint64_t> steps_; // Number of steps to go
  bool infinite_rotation_; // If true, ignore steps_ and rotate infinitely
  bool clockwise_; // Clockwise or anti-clockwise
  std::chrono::nanoseconds step_delay_; // Time between each step in nano seconds
  std::atomic<bool> stop_stepping_; // Set true to stop the motor
  std::chrono::nanoseconds start_delay_; // Delay before starting moving

  // Multi-threading + safety
  std::thread worker_;
  std::atomic<bool> is_locked_;
};

#endif
